(function() {

    var ViewsLoginApp = angular.module("ViewsLoginApp", ["http-auth-interceptor", "ui.router"]);
    
    var LoginCtrl = function($http, $httpParamSerializerJQLike, authService) {
        var vm = this;
        vm.username = "";
        vm.password = "";
        vm.login = function() {
            $http({
                url: "/authenticate",
                method: "POST",
                data: $httpParamSerializerJQLike({
                    username: vm.username,
                    password: vm.password
                }),
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                }
            }).then(function() {
                authService.loginConfirmed(vm.username);
            });
        }
    };
    
    var MenuCtrl = function($http, $state, authService) {
        var vm = this;
        vm.logout = function () {
            $http.get("/logout")
                .then(function() {
                    $state.go("main");
                    authService.loginCancelled();
                });
        }
    }
    
    var ViewsLoginConfig = function($httpProvider, $stateProvider, $urlRouterProvider) {
        $httpProvider.defaults.withCredentials = true;
        $stateProvider
            .state("main", {
                url: "/main",
                templateUrl: "/views/main.html"
            })
            .state("login", {
                url: "/login",
                templateUrl: "/views/login.html",
                controller: ["$http", "$httpParamSerializerJQLike", "authService", LoginCtrl],
                controllerAs: "loginCtrl"
            })
            .state("protected_menu", {
                url: "/protected/menu",
                templateUrl: "/protected/views/menu.html",
                controller: ["$http", "$state", "authService", MenuCtrl],
                controllerAs: "menuCtrl"
            });
        
        $urlRouterProvider.otherwise("/main");
    }
    
    var ViewsLoginCtrl = function($scope, $state, authService) {
        
        var vm = this;
        
        vm.status = {
            message: ""
        }
        
        //401
        $scope.$on("event:auth-loginRequired", function() {
            vm.status.message = "Please login";
            $state.go("login");
        });
        //200
        $scope.$on("event:auth-loginConfirmed", function(_, name) {
            vm.status.message = "Hello " + name;
            $state.go("protected_menu");
        });
        //403
        $scope.$on("event:auth-forbidden", function() {
            vm.status.message = "Please username/password. Please try again";
            authService.loginConfirmed();
        });
        $scope.$on("event:auth-loginCancelled", function() {

        });
        
    }
    
    ViewsLoginApp.config(["$httpProvider", "$stateProvider", "$urlRouterProvider", ViewsLoginConfig]);
    ViewsLoginApp.controller("ViewsLoginCtrl", ["$scope", "$state", "authService", ViewsLoginCtrl]);
})();